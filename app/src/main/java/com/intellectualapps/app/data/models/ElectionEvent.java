package com.intellectualapps.app.data.models;


public class ElectionEvent {

    private String id;
    private String agentId;
    private String electionDate;
    private String electionId;
    private String electionType;
    private String username;


    private String activeElectionEvent;
    private String date;

    public ElectionEvent(String electionType, String electionDate){
        this.electionType = electionType;
        this.electionDate = electionDate;
    }

    public String getActiveElectionEvent() {
        return activeElectionEvent;
    }

    public void setActiveElectionEvent(String activeElectionEvent) {
        this.activeElectionEvent = activeElectionEvent;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getElectionDate() {
        return electionDate;
    }

    public void setElectionDate(String electionDate) {
        this.electionDate = electionDate;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getElectionType() {
        return electionType;
    }

    public void setElectionType(String electionType) {
        this.electionType = electionType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
