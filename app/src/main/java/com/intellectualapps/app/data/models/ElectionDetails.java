package com.intellectualapps.app.data.models;

public class ElectionDetails {
    private String electionType;
    private int electionCount;
    public ElectionDetails(String electionType, int electionCount){
        this.electionType = electionType;
        this.electionCount = electionCount;
    }

    public String getElectionType() {
        return electionType;
    }

    public void setElectionType(String electionType) {
        this.electionType = electionType;
    }

    public int getElectionCount() {
        return electionCount;
    }

    public void setElectionCount(int electionCount) {
        this.electionCount = electionCount;
    }
}
