package com.intellectualapps.app.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.intellectualapps.app.R;

public class VotersAndVotesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_voters_and_votes);
    }

    public VotersAndVotesActivity(){
    }

    public void nextScreen(View view){
        Intent intent=new Intent(this, ElectionDetailsActivity.class);
        startActivity(intent);
    }
}
