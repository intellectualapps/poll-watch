package com.intellectualapps.app.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.intellectualapps.app.R;

public class ConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_confirmation);
    }

    public void voters(View view){
        Intent intent=new Intent(this, VotersAndVotesActivity.class);
        startActivity(intent);
    }

    public void clear(View view){
        Intent intent=new Intent(this, VotersAndVotesActivity.class);
        startActivity(intent);
    }

    public void Vote(View view) {
        Intent intent=new Intent(this, VotersAndVotesActivity.class);
        startActivity(intent);
    }

    public void party(View view) {
        Intent intent=new Intent(this, ElectionDetailsActivity.class);
        startActivity(intent);
    }

    public void result(View view) {
        Intent intent=new Intent(this, ResultSheetPhotoActivity.class);
        startActivity(intent);
    }

    public void election(View view) {
        Intent intent=new Intent(this, ElectionPhotoActivity.class);
        startActivity(intent);
    }

    public void submit(View view) {
        Intent intent=new Intent(this, FilledFormsActivity.class);
        startActivity(intent);
    }
}
