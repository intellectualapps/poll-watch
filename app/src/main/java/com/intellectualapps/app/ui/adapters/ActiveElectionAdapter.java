package com.intellectualapps.app.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.intellectualapps.app.R;
import com.intellectualapps.app.data.models.ElectionEvent;
import com.intellectualapps.app.databinding.ActiveElectionListBinding;
import com.intellectualapps.app.ui.activities.VotersAndVotesActivity;

import java.util.ArrayList;
import java.util.List;

public class ActiveElectionAdapter extends RecyclerView.Adapter<ActiveElectionAdapter.ViewHolder> {

    private List<ElectionEvent> electionEvents;
    private ItemClickListener clickListener;

    public ActiveElectionAdapter(List<ElectionEvent> electionEvents){
        this.electionEvents = electionEvents;
    }

    @NonNull
    @Override
    public ActiveElectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View statusContainer = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_election_list, parent, false);
        return new ActiveElectionAdapter.ViewHolder(statusContainer);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindUser(electionEvents.get(position));
    }

    public interface ItemClickListener {
        void onClick(View view, int position);
    }

    @Override
    public int getItemCount() {
        return electionEvents.size();
    }

    public void setClickListener(ItemClickListener itemClickListener){
        this.clickListener = itemClickListener;
    }
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ActiveElectionListBinding binding;
        private String mItem;
        private TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            Context context = view.getContext();
            Intent intent = new Intent(context, VotersAndVotesActivity.class);
            context.startActivity(intent);
        }

        public void bindUser(final ElectionEvent user){
            binding.setActiveElection(user);
        }
    }
}
