package com.intellectualapps.app.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intellectualapps.app.R;
import com.intellectualapps.app.data.models.ElectionEvent;

import com.intellectualapps.app.databinding.ActiveElectionListBinding;

import com.intellectualapps.app.databinding.FilledFormsListBinding;

import java.util.ArrayList;

public class FilledFormsAdapter extends RecyclerView.Adapter<FilledFormsAdapter.ViewHolder> {

    private ArrayList<ElectionEvent> list;

    public FilledFormsAdapter(ArrayList<ElectionEvent> list){
        this.list = list;
    }

    @NonNull
    @Override
    public FilledFormsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View statusContainer = LayoutInflater.from(parent.getContext()).inflate(R.layout.filled_forms_list, parent, false);
        return new FilledFormsAdapter.ViewHolder(statusContainer);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindUser(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        private FilledFormsListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
        public void bindUser(final ElectionEvent user){
            binding.setActiveElection(user);
        }
    }
}

