package com.intellectualapps.app.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intellectualapps.app.R;
import com.intellectualapps.app.data.models.ElectionDetails;
import com.intellectualapps.app.databinding.ItemElectionDetailsBinding;

import java.util.ArrayList;


public class ElectionDetailsAdapter extends RecyclerView.Adapter<ElectionDetailsAdapter.ViewHolder> {

    private ArrayList<ElectionDetails> list;
    public ElectionDetailsAdapter(ArrayList<ElectionDetails> list){
        this.list = list;
    }


    @NonNull
    @Override
    public ElectionDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemElectionDetailsBinding binding = ItemElectionDetailsBinding.inflate(inflater, parent, false);
        return new ElectionDetailsAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindUser(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        private ItemElectionDetailsBinding binding;

        ViewHolder(ItemElectionDetailsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bindUser(ElectionDetails electionDetails){
            binding.setElectionDetails(electionDetails);
        }
    }
}
